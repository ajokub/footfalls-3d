// @flow
// $FlowIgnore
import R from 'ramda';
import type {
  CoordArr,
  ColorArr,
  Color,
  SplitArray,
  IndexArr,
} from './dataTypes.flow.js';

/**
 * Generate colors for all the vertices
 */
function generate(
  arr: CoordArr,
  baseColor: Array<number> = [1, 1, 1, 1]
): ColorArr {
  if (baseColor.length !== 4) {
    throw Error('baseColor has to be of length 4');
  }

  const newArr = [];
  const length = arr.length / 3;

  for (let i = 0; i < length; i += 1) {
    newArr.push(...baseColor);
  }
  return newArr;
}
/* eslint-disable no-param-reassign */
/**
 * Will add the color to a face containing given vertices
 */
function face(
  vertices: IndexArr,
  colors: ColorArr,
  color: Color
): ColorArr {
  const pointer = i => i * 4;
  const length = vertices.length;
  for (let i = 0; i < length; i += 1) {
    // Basically color array is groups of 4 instaed of 3 like others are
    // As we split the arrays before indexes are divided by 3 already
    // So we just multiply by 4
    const p = pointer(vertices[i]);
    colors[p] = color.r;
    colors[p + 1] = color.g;
    colors[p + 2] = color.b;
    colors[p + 3] = color.lum || 1;
  }
  return colors;
}
/* eslint-enable no-param-reassign */
export {
  generate,
  face,
};

export default {
  generate,
  face,
};
