import RenderEngine from './containers/RenderEngine';
import { footData } from './helpers/data';


const root = document.getElementById('root');
const engine = new RenderEngine(root);
engine.render();

function changeColor({ colorFn, nodeList }) {
  function loop(i) {
    if (i === nodeList.length) return;
    const node = nodeList[i].node;
    const color = colorFn(nodeList[i].value);
    if (node !== 9) {
      engine.pressureChange(node, color);
    }
    setTimeout(() => loop(i + 1), 100);
  }
  loop(0);
}

// footData().then(changeColor);

if (module.hot) {
  module.hot.accept('./containers/RenderEngine', () => {
    // engine.reRender();
    window.location.reload();
  });
}
