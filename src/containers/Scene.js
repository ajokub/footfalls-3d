// @flow
/* eslint-disable */
// $FlowIgnore
import * as BABYLON from 'babylonjs';
// $FlowIgnore
import R from 'ramda';

import control from '../debug';
import mergeMeshes from '../helpers/mergeMashes';
import splitToThree from '../helpers/splitToThree';
import {
  colorIndexes,
  setColorAt,
} from '../helpers/geometry';

import colorH from '../helpers/color';
import indiceH from '../helpers/indices';
import verticeH from '../helpers/vertices';

// let TEMP_COORD = {x: -1.0752, y: 0.3059, z: -1.042}
const arrCord = [1.7374, 4.4372, -0.8365];
let TEMP_COORD =  {
  x: -0.7847,
  y: 0.5022,
  z: -0.2778
};
// let TEMP_COORD = {x: arrCord[0], y: arrCord[1], z: arrCord[2]};
const TEMP_COLOR = {
  r: 1,
  g: 0,
  b: 0,
};

function addSphere(scene, { x, y, z }: Object, diameter: number = 0.05, color = new BABYLON.Color3(0, 0, 0)) {
  const sphere = BABYLON.MeshBuilder.CreateSphere("sphere", { diameter }, scene);
  const material = new BABYLON.StandardMaterial("texture1", scene);
  
  material.diffuseColor =  color;
  sphere.position = new BABYLON.Vector3(x, y, z);
  sphere.material = material;
}

function extractData(scene, mesh) {
  const verticeData = (key: string, alt: Array<*> = []): Array<*> =>
    mesh.getVerticesData(BABYLON.VertexBuffer[key]) || alt;

  const positionData = verticeData('PositionKind');
  let colorList = verticeData('ColorKind', colorH.generate(positionData));

  const positions = splitToThree(positionData);
  const indexList = splitToThree(mesh.getIndices())

  // We are extracting all of the nearby vertices
  const verticesNear = verticeH.similarTo(TEMP_COORD, positions, { x: 0.05, y: 0.01, z: 0.05 })
  const relatedIndeces = indiceH.list(indexList,verticesNear);
  relatedIndeces.map(index => {
    const verticeGroup = verticeH.fromIndice(indexList, index);
    colorList = colorH.face(verticeGroup, colorList, TEMP_COLOR);
  });


  const vertexData = new BABYLON.VertexData();
  vertexData.colors = colorList;
  vertexData.applyToMesh(mesh, true);
}
const importMesh = (scene: Object, name: string) => new Promise((res) => {
  BABYLON.SceneLoader.ImportMesh('', './static/models/', name, scene, res);
})

export default class Scene {
  _scene: BABYLON.Scene;
  _camera: BABYLON.ArcRotateCamera;
  _light: BABYLON.HemisphericLight;
  materials: Object;

  constructor(engine: BABYLON.Engine, canvas: HTMLElement) {
    const scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color4(0.5, 0.8, 0.6, 0.8);

    const camera = new BABYLON.ArcRotateCamera('Camera', 1.5 * Math.PI, Math.PI / 8, 50, BABYLON.Vector3.Zero(), scene);
    camera.setTarget(new BABYLON.Vector3(0, 0, 0));
    camera.setPosition(new BABYLON.Vector3(-5, 5, -2));
    camera.attachControl(canvas, false);
    camera.wheelPrecision = 50;

    const light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(1,10, 10), scene);
    light.intensity = 1;

    const ligh2 = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(10,10, -10), scene);
    ligh2.intensity = 1;


    Promise.all([ importMesh(scene, 'feet.babylon') ])
    .then(this.extractMaterials)
    .catch(e => console.log(e));

    // MouseEvents
    // Need to rember to add onDispose to remove listeners
    var onPointerDown = function (evt) {
        var pickInfo = scene.pick(scene.pointerX, scene.pointerY);
        if (pickInfo.hit) {
            console.log(pickInfo.pickedPoint);
            addSphere(scene, pickInfo.pickedPoint);
        }
    }
    window.addEventListener("click", onPointerDown);

    // scene.debugLayer.show();
    this._scene = scene;
    this._camera = camera;
  }
  extractMaterials = (data: Array<Object>) => {
    console.log(data);

    const mesh = data[0][0];

    const vertexData = new BABYLON.VertexData();
    vertexData.positions =  mesh.getVerticesData(BABYLON.VertexBuffer.PositionKind);
    vertexData.applyToMesh(mesh, true);    

    extractData(this._scene, mesh);
  }
  // changeColor = (key: string,{ r, g , b }: Color) => {
  //   try {
  //     this.materials[key].diffuseColor = new BABYLON.Color3(r, g, b);
  //   } catch (e) {
  //     console.log(key, this.materials);
  //   }
  // } 
  render = () => {
    this._scene.render();
  }
}
