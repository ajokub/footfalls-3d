'use stricter';

import { Engine, SceneLoader } from 'babylonjs';
import Scene from './Scene';

export default class RenderEngine {
  constructor(container) {
    this._container = container;
  }
  animate() {
    this._engine.runRenderLoop(() => this._scene.render());
    window.addEventListener('resize', () => this._engine.resize);
  }
  createScene() {
    this._scene = new Scene(this._engine, this._canvas);
  }
  pressureChange = (...args) => {
    this._scene.changeColor(...args);
  }
  render = () => {
    this._canvas = document.createElement('canvas');
    this._container.appendChild(this._canvas);
    this._engine = new Engine(this._canvas, false);
    this.createScene();
    this.animate();
  }
  reRender = () => {
    this._engine.dispose();
    this._container.removeChild(this._canvas);
    console.log('hello');
    this.render();
  }
}
