// @flow
export type NumArr = Array<number>;

export type Coord = {
  x: number,
  y: number,
  z: number,
};

export type SplitArray = {
  f: NumArr,
  s: NumArr,
  t: NumArr,
};

export type Color = {
  r: number,
  g: number,
  b: number,
  lum?: number,
};

export type IndexArr = NumArr;
export type CoordArr = NumArr;
export type ColorArr = NumArr;
