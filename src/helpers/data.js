import R from 'ramda';
import { interpolateRdYlBu } from 'd3-scale-chromatic';
import { scaleSequential } from 'd3';

const cmp = fn => (value, { value: value2 }) => {
  try {
    return fn(value, value2) ? value : value2;
  } catch (e) {
    return value2;
  }
};
const transform = {
  timestamp: x => parseFloat(x),
  node: x => parseFloat(x),
  value: x => parseFloat(x),
};
const normalize = R.map(R.evolve(transform));

const maxFn = (v1, v2) => v1 > v2;
const minFn = (v1, v2) => v1 < v2;

const colorRegExp = /rgb\((.*),(.*),(\s.*)\)/g;
const toOneScale = rgb => Object.assign({}, {
  r: rgb.r / 255,
  g: rgb.g / 255,
  b: rgb.b / 255,
});

const colorToObj = R.compose(toOneScale, JSON.parse, R.replace(colorRegExp, '{"r":$1,"g":$2,"b":$3}'));

export async function footData() {
  const req = await fetch('/static/right.json');
  const data = await req.json();
  const nodeList = normalize(data.node_list);
  const max = R.reduce(cmp(maxFn), 0, nodeList);
  const min = R.reduce(cmp(minFn), max, nodeList);
  // Scale should be dinamically updated
  const colorScale = scaleSequential(interpolateRdYlBu)
      .domain([max, min]);// Is reverted as lower values should be colder
  const colorFn = R.compose(colorToObj, colorScale);
  return {
    colorFn,
    nodeList,
  };
}
