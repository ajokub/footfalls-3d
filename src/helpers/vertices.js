// @flow

import R from 'ramda';
import roundTo from 'round-to';

import type {
  Coord,
  Color,
  SplitArray,
  IndexArr,
  CoordArr,
  ColorArr,
  NumArr,
} from './dataTypes.flow.js';

/**
 * Returns indices of vertices that are closer than offset
 */
function similarTo(coord: Coord, vertices: SplitArray, offset: Coord): IndexArr {
  const isNear = (i, k1, k2) =>
    Math.abs(vertices[k1][i] - coord[k2]) <= offset[k2];

  const closeBy = [];
  const length = vertices.f.length;

  for (let i = 0; i < length; i += 1) {
    if (isNear(i, 'f', 'x') && isNear(i, 's', 'y') && isNear(i, 't', 'z')) {
      closeBy.push(i);
    }
  }
  return closeBy;
}
/**
 * Takes and indice returns index of each vertice that it contains
 */
function fromIndice(indices: SplitArray, index: number): IndexArr {
  const take = key => indices[key][index];
  return [take('f'), take('s'), take('t')];
}
export {
  similarTo,
  fromIndice
};

export default {
  similarTo,
  fromIndice
};
