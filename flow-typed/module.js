// @flow

declare var module : {
  hot: {
    accept(path: string, callback: () => void): void;
  };
};

declare module 'ramda' {
  declare module.exports: any;
}
