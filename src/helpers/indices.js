// @flow
import R from 'ramda';
import type {
  IndexArr,
  SplitArray,
} from './dataTypes.flow.js';

// Note should be moved to the object, so it can be saved, rather than m
/**
 * Returns all indices that use the vertice
 */
function fromA(indiceArr: SplitArray): ((vertixIndex: number) => IndexArr) {
  const isEq = (val, i, key) => val === indiceArr[key][i];
  const length = indiceArr.f.length;

  function contains(index) {
    const indexArr = [];
    for (let i = 0; i < length; i += 1) {
      if (isEq(index, i, 'f') || isEq(index, i, 'f') || isEq(index, i, 'f')) {
        indexArr.push(i);
      }
    }
    return indexArr;
  }
  return R.memoize(contains);
}
/**
 * Returns all indices for each given vertice
 */
function list(indiceArr: SplitArray, vertices: IndexArr): IndexArr {
  const find = fromA(indiceArr);
  const indices = [];
  const length = vertices.length;

  for (let i = 0; i < length; i += 1) {
    const found = find(vertices[i]);
    indices.push(...found);
  }

  return indices;
}

export {
  list,
};

export default {
  list,
};
