// @flow
// $FlowIgnore
import R from 'ramda';
// $FlowIgnore
import roundTo from 'round-to';
// $FlowIgnore
import { VertexBuffer } from 'babylonjs';

import type {
  Coord,
  Color,
  SplitArray,
  IndexArr,
  CoordArr,
  ColorArr,
  NumArr,
} from './dataTypes.flow.js';

window.roundTo = roundTo;

function nearByVals(arr: CoordArr, val: any, round: Function): IndexArr {
  // Could possibly be optimised by using external array
  return arr
    .map((v, i) => (round(val) === round(v) ? i : -1))
    // .map((v, i) => {
    //   if (v < -1.07 && v > -1.08) {
    //     console.log(`${round(val)} : ${round(v)}`);
    //     console.log(`${val} : ${v}`);
    //     console.log('-------');
    //   }
    //   return round(val) === round(v) ? i : -1;
    // })
    .filter(n => n !== -1);
}

/**
 * Finds list of vertices,
 * Which are closest to a given co-ordinate
 */
export function verticesNear(coord: Coord, coordDict: SplitArray, decimalPoint: number): IndexArr {
  const round = R.memoize(n => roundTo(n, decimalPoint));
  const isNear = (i, k1, k2) =>
    R.eqBy(round, coordDict[k1][i], coord[k2]);
  // We start search with x, but could be done by y or z
  const indexes = nearByVals(coordDict.f, coord.x, round);
  return indexes
    .filter(i => isNear(i, 's', 'y') && isNear(i, 't', 'z'));
}


export function verticeData(mesh: Object, key: string, alt: ?Array<*>): Array<*> {
  return mesh.getVerticesData(VertexBuffer[key]) || alt || [];
}

export function colorIndexes(indices: SplitArray, index: number): IndexArr {
  const take = key => indices[key][index];
  return [take('f'), take('s'), take('t')];
}
export function setColorAt(i: number, color: Color, colorArr: NumArr): NumArr {
  const colors = colorArr.slice();
  const p = i * 4;
  colors[p] = color.r;
  colors[p + 1] = color.g;
  colors[p + 2] = color.b;
  colors[p + 3] = color.lum || 1;
  return colors;
}
