// @flow
import type { SplitArray } from './dataTypes.flow.js';

/**
 * Will split array to three different ones to speed up iliteration
 */
export default function splitToThree(arr: Array<*>): SplitArray {
  // Because array is just groups of 3
  const lenght = arr.length / 3;
  const f = [];
  const s = [];
  const t = [];
  // Prealocating can improve performance
  f.length = lenght;
  s.length = lenght;
  t.length = lenght;

  for (let i = 0; i < lenght; i += 1) {
    const p = i * 3;
    f[i] = arr[p];
    s[i] = arr[p + 1];
    t[i] = arr[p + 2];
  }
  return { f, s, t };
}
